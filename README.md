# dcwebui2

![](https://img.shields.io/badge/written%20in-Javascript%20%28node.js%2C%20socket.io%29-blue)

A web interface to an NMDC hub.

Chat in real-time on your NMDC hub using a web browser. Now that websockets are reasonably mature, this deprecates the earlier [flexdc](https://code.ivysaur.me/flexdc/) (flash sockets) and [dcwebui](https://code.ivysaur.me/dcwebui/) (c++/polling) projects, but falls back to other COMET techniques for older browsers (including flashsockets, forever-iframe, and polling).

This project has been superceded by [nmdc-webfrontend](https://code.ivysaur.me/nmdc-webfrontend/).

## Features

- Suitable for desktop use or mobile use, with a responsive web interface and iOS-inspired theme
- Nick autocompletion
- Multiple tabs for PM windows
- Right-click commands

## Usage

1. Get the latest version from npm: `npm install dcwebui`
2. Copy dcwebui-conf.js.sample to dcwebui-conf.js and tweak the settings.
3. Run the server with node ./dcwebui-server.js

Npm project page: https://npmjs.org/package/dcwebui 

Tags: nmdc

## Changelog

Unreleased 1.4.x
- Require http/s configuration in the configuration file
- Fix a cosmetic issue with the Wii U internet browser

2013-11-16 1.3.0a
- Fix a cosmetic issue with Internet Explorer

2013-11-16 1.3.0
- Feature: Corner application menu
- Feature: Option to toggle displaying join/part messages
- Feature: UserCommand support
- Enhancement: Update nmdc.js to `>=1.2.0`
- Fix an issue with tab completion and scrollbars in Firefox
- Fix a cosmetic issue with duplicate users appearing

2013-10-10 1.2.0
- Feature: Tab completion
- Feature: Double-click nick to PM
- Enhancement: Provide sample configuration file
- Fix an issue with browser autocomplete on the input area
- Fix an issue with nicks in the userlist
- Fix a cosmetic issue with sticking to non-existing userlist tab when resizing out of the mobile responsive layout
- Fix a cosmetic issue with the tab close button

2013-07-02 1.1.0c
- Fix an issue with TypeError in iOS Safari

2013-05-23 1.1.0b
- Feature: Support passworded nicks
- Feature: Support multiple PM tab windows
- Feature: Responsive userlist design
- Update nmdc.js from `1.0.1a` to `*`
- Fix an issue with title and CSS in Firefox

2013-03-26 1.1.0a
- Feature: Redesign to use multiple tab panes

2013-03-26 1.0.0d
- Initial development milestone


## Download

- [⬇️ dcwebui-1.3.0.tgz](dist-archive/dcwebui-1.3.0.tgz) *(32.51 KiB)*
- [⬇️ dcwebui-1.2.0.tgz](dist-archive/dcwebui-1.2.0.tgz) *(30.41 KiB)*
- [⬇️ dcwebui-1.1.0b.tgz](dist-archive/dcwebui-1.1.0b.tgz) *(29.24 KiB)*
- [⬇️ dcwebui-1.1.0a.tgz](dist-archive/dcwebui-1.1.0a.tgz) *(29.27 KiB)*
- [⬇️ dcwebui-1.0.0b.tgz](dist-archive/dcwebui-1.0.0b.tgz) *(26.89 KiB)*
